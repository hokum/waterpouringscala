package WaterPouring

object Main extends App {
  //val problem = new Pouring(Vector(4, 6, 19, 33, 55))
  //val target = 65
  val problem = new Pouring(Vector(4, 6, 19, 33))
  val target = 6
  
  {
    val now = System.nanoTime
    val solution = problem.solution(target)
    val mili = (System.nanoTime - now) / 1000000;
    println("%d ms".format(mili))
    if (solution.isEmpty)
      println("solution was not found")
    else
      println(solution.head)
  }

  {
    val now = System.nanoTime
    val solution = problem.solution(target)
    val mili = (System.nanoTime - now) / 1000000;
    println("%d ms".format(mili))
    if (solution.isEmpty)
      println("solution was not found")
    else
      println(solution.head)
  }
}
